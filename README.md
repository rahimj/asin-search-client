#### ASIN Search Client

This is a basic client for the ASIN Search API (found here: ) 
This has been built on `React` and `Redux` using boilerplate from: https://github.com/react-boilerplate. For more information on architecture decisions and designs, please check out the repo.

All styling has been taken care of courtesy of https://react-bootstrap.github.io/.

#### Setup
* Ensure you have `npm` or `yarn` installed.
* Install dependencies usin `npm install` or `yarn install`.
* Run `npm start` or `yarn start` to get going.
* Content will be served on `http://localhost:3000/`

#### TODOs
* Fix specs on components.
* Use `styled-components`.

#### Screen Shot
![Alt text](app/images/banana_baby.png?raw=true "")

![Alt text](app/images/not_found.png?raw=true "")
