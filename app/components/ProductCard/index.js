/**
*
* ProductCard
*
*/

import React, { PureComponent, PropTypes } from 'react';
import { Table } from 'react-bootstrap';

class ProductCard extends PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { asin, categories, description, dimensions, name, rank, rating } = this.props;
    return (
      <span>
        <h4>{name}</h4>
        <Table responsive>
          <thead>
            <tr>
              <th>ASIN</th>
              <th>Categories</th>
              <th>Description</th>
              <th>Dimensions</th>
              <th>Rank</th>
              <th>Rating</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td style={{ maxWidth: '10px' }}>{asin}</td>
              <td style={{ maxWidth: '50px' }}>{categories.join(', ')}</td>
              <td style={{ maxWidth: '200px' }} >{description}</td>
              <td style={{ maxWidth: '10px' }}>{dimensions}</td>
              <td style={{ maxWidth: '80px' }}>{rank}</td>
              <td style={{ maxWidth: '10px' }}>{rating}</td>
            </tr>
          </tbody>
        </Table>
      </span>
    );
  }
}
ProductCard.propTypes = {
  asin: PropTypes.string,
  categories: PropTypes.map,
  description: PropTypes.string,
  dimensions: PropTypes.string,
  name: PropTypes.string,
  rank: PropTypes.string,
  rating: PropTypes.string,
};

export default ProductCard;
