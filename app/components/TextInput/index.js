/**
*
* TextInput
*
*/

import React, { Component, PropTypes } from 'react';
import { FormGroup, HelpBlock, FormControl, InputGroup, Button } from 'react-bootstrap';

class TextInput extends Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { value: '' };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const { value } = event.target;
    this.setState({ value });
    this.props.onChange(value);
  }

  handleSubmit(event) {
    this.props.onSubmit();
    this.setState({ value: '' });
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <FormGroup
          controlId="formBasicText"
          validationState={this.props.errorMessage !== '' ? 'error' : null}
        >
          <InputGroup>
            <FormControl
              type="text"
              value={this.state.value}
              placeholder="ASIN Goes Here :)"
              onChange={this.handleChange}
            />
            <InputGroup.Button>
              <Button type="submit">Submit</Button>
            </InputGroup.Button>
          </InputGroup>
          <HelpBlock>{this.props.errorMessage}</HelpBlock>
        </FormGroup>
      </form>
    );
  }
}

TextInput.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  errorMessage: PropTypes.string,
};

export default TextInput;
