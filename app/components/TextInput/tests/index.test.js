import React from 'react';
import { shallow } from 'enzyme';

import { FormGroup, HelpBlock, FormControl, InputGroup, Button } from 'react-bootstrap';
import TextInput from '../index';

describe('<TextInput />', () => {
  const renderedComponent = shallow(
    <TextInput onSubmit={() => {}} onChange={() => {}} />
  );
  expect(renderedComponent.contains(
    <FormGroup />
  )).toEqual(true);
  expect(renderedComponent.contains(
    <HelpBlock />
  )).toEqual(true);
  expect(renderedComponent.contains(
    <FormControl />
  )).toEqual(true);
  expect(renderedComponent.contains(
    <InputGroup />
  )).toEqual(true);
  expect(renderedComponent.contains(
    <Button />
  )).toEqual(true);
});
