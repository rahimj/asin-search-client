/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import TextInput from 'components/TextInput';
import ProductCard from 'components/ProductCard';
import headerImage from 'images/header_image.png';
import { loadProduct, changeASIN } from './actions';
import reducer from './reducer';
import saga from './saga';
import {
  makeSelectLoadedASIN,
  makeSelectASIN,
  makeSelectErrorMessage,
  makeSelectCategories,
  makeSelectDescription,
  makeSelectDimensions,
  makeSelectName,
  makeSelectRank,
  makeSelectRating,
  makeSelectProductFound,
} from './selectors';

export class HomePage extends Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      onSubmitForm,
      onChangeASIN,
      errorMessage,
      loadedAsin,
      categories,
      description,
      dimensions,
      name,
      rank,
      rating,
      productFound,
    } = this.props;

    let product;

    if (productFound) {
      product = (<ProductCard
        asin={loadedAsin}
        categories={categories}
        description={description}
        dimensions={dimensions}
        name={name}
        rank={rank}
        rating={rating}
      />);
    }

    const imageStyle = {
      display: 'block',
      marginLeft: 'auto',
      marginRight: 'auto',
      width: '50 %',
      maxWidth: '400px',
      padding: '20px',
    };

    return (
      <div>
        <img src={headerImage} style={{ ...imageStyle }} alt="ASIN Search" />
        <TextInput
          onSubmit={onSubmitForm}
          onChange={onChangeASIN}
          errorMessage={errorMessage}
        />
        {product}
      </div>
    );
  }
}

export function mapDispatchToProps(dispatch) {
  return {
    onSubmitForm: (evt) => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(loadProduct());
    },
    onChangeASIN: (value) => {
      dispatch(changeASIN(value));
    },
  };
}

const mapStateToProps = createStructuredSelector({
  asin: makeSelectASIN(),
  loadedAsin: makeSelectLoadedASIN(),
  errorMessage: makeSelectErrorMessage(),
  categories: makeSelectCategories(),
  description: makeSelectDescription(),
  dimensions: makeSelectDimensions(),
  name: makeSelectName(),
  rank: makeSelectRank(),
  rating: makeSelectRating(),
  productFound: makeSelectProductFound(),
});

HomePage.propTypes = {
  onSubmitForm: PropTypes.func.isRequired,
  onChangeASIN: PropTypes.func.isRequired,
  loadedAsin: PropTypes.string,
  errorMessage: PropTypes.string,
  categories: PropTypes.array,
  description: PropTypes.string,
  dimensions: PropTypes.string,
  name: PropTypes.string,
  rank: PropTypes.string,
  rating: PropTypes.string,
  productFound: PropTypes.bool,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
