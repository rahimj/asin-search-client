/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';

const selectHome = (state) => state.get('home');

const makeSelectLoadedASIN = () => createSelector(
  selectHome,
  (homeState) => homeState.get('loadedAsin')
);

const makeSelectASIN = () => createSelector(
  selectHome,
  (homeState) => homeState.get('asin')
);

const makeSelectErrorMessage = () => createSelector(
  selectHome,
  (homeState) => homeState.get('errorMessage')
);

const makeSelectCategories = () => createSelector(
  selectHome,
  (homeState) => homeState.get('product').categories
);
const makeSelectDescription = () => createSelector(
  selectHome,
  (homeState) => homeState.get('product').description
);
const makeSelectDimensions = () => createSelector(
  selectHome,
  (homeState) => homeState.get('product').dimensions
);
const makeSelectName = () => createSelector(
  selectHome,
  (homeState) => homeState.get('product').name
);
const makeSelectRank = () => createSelector(
  selectHome,
  (homeState) => homeState.get('product').rank
);
const makeSelectRating = () => createSelector(
  selectHome,
  (homeState) => homeState.get('product').rating
);
const makeSelectProductFound = () => createSelector(
  selectHome,
  (homeState) => homeState.get('productFound')
);

export {
  selectHome,
  makeSelectLoadedASIN,
  makeSelectASIN,
  makeSelectErrorMessage,
  makeSelectCategories,
  makeSelectDescription,
  makeSelectDimensions,
  makeSelectName,
  makeSelectRank,
  makeSelectRating,
  makeSelectProductFound,
};
