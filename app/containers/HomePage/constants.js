/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const LOAD_PRODUCT = 'boilerplate/HomePage/LOAD_PRODUCT';
export const LOAD_PRODUCT_SUCCESS = 'boilerplate/HomePage/LOAD_PRODUCT_SUCCESS';
export const LOAD_PRODUCT_ERROR = 'boilerplate/HomePage/LOAD_PRODUCT_ERROR';
export const CHANGE_ASIN = 'boilerplate/HomePage/CHANGE_ASIN';
