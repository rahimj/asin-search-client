import { fromJS } from 'immutable';

import {
  LOAD_PRODUCT,
  LOAD_PRODUCT_SUCCESS,
  LOAD_PRODUCT_ERROR,
  CHANGE_ASIN,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  loading: false,
  errorMessage: '',
  asin: '',
  loadedAsin: '',
  product: {},
  products: [],
  productFound: false,
});

function homeReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_PRODUCT:
      return state
        .set('errorMessage', '')
        .set('productFound', false)
        .set('loading', true);
    case LOAD_PRODUCT_SUCCESS:
      return state
        .set('asin', '')
        .set('loading', false)
        .set('product', action.product)
        .set('productFound', true)
        .set('loadedAsin', action.asin)
        .setIn(['products', state.get('products').size], action.product);
    case LOAD_PRODUCT_ERROR:
      return state
        .set('product', {})
        .set('errorMessage', action.error.message)
        .set('productFound', false)
        .set('loading', false);
    case CHANGE_ASIN:
      return state
        .set('asin', action.asin);
    default:
      return state;
  }
}

export default homeReducer;
