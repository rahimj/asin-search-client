/**
 * Gets the repositories of the user from Github
 */

import { call, put, select, takeLatest } from 'redux-saga/effects';
import { LOAD_PRODUCT } from 'containers/HomePage/constants';
import { productLoaded, productLoadingError } from 'containers/HomePage/actions';

import request from 'utils/request';
import { makeSelectASIN } from './selectors';

/**
 * Github repos request/response handler
 */
export function* getProduct() {
  // Select username from store
  const asin = yield select(makeSelectASIN());
  const requestURL = `http://localhost:9000/products/${asin}`;

  try {
    const product = yield call(request, requestURL);
    yield put(productLoaded(asin, product));
  } catch (err) {
    yield put(productLoadingError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* productData() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOAD_PRODUCT, getProduct);
}
