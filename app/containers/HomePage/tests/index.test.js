import React from 'react';
import { shallow } from 'enzyme';

import TextInput from 'components/TextInput';
import ProductCard from 'components/ProductCard';
import HomePage from '../index';

describe('<HomePage />', () => {
  const defaultProps = {
    onSubmitForm: () => {},
    onChangeASIN: () => {},
    loadedAsin: 'foo',
    errorMessage: 'foo',
    categories: ['foo'],
    description: 'foo',
    dimensions: 'foo',
    name: 'foo',
    rank: 'foo',
    rating: 'foo',
    productFound: false,
  };

  it('should render the TextInput but not the ProductCard without the product loaded', () => {
    const renderedComponent = shallow(
      <HomePage {...defaultProps} />
    );
    expect(renderedComponent.contains(
      <TextInput />
    )).toEqual(true);
    expect(renderedComponent.contains(
      <ProductCard />
    )).toEqual(false);
  });

  it('should render the TextInput and ProductCard with the product loaded', () => {
    const renderedComponent = shallow(
      <HomePage {...defaultProps} productLoaded={false} />
    );
    expect(renderedComponent.contains(
      <TextInput />
    )).toEqual(true);
    expect(renderedComponent.contains(
      <ProductCard />
    )).toEqual(true);
  });
});
